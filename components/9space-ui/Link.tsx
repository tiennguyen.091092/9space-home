import React from 'react'
import LinkMUI, { LinkProps } from '@mui/material/Link'
import { SxProps, Theme } from '@mui/system'

export interface ILinkProps extends LinkProps {}
const Link: React.FC<ILinkProps> = (props) => {
    const LinkStyle: SxProps<Theme> = {
            textDecoration:"none"
    }
    return (
        <LinkMUI
            underline="none"
            {...props}
            sx={{ ...LinkStyle, ...props.sx }}
        ></LinkMUI>
    )
}

export default Link
