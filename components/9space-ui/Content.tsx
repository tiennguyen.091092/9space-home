import * as React from 'react'
import BoxMUI, { BoxProps } from '@mui/material/Box'

export interface IContentProps extends BoxProps {

}

const Content: React.FC<IContentProps> = (props) => {
    return (
        <BoxMUI
            {...props}
            sx={{
                width: '100%',

                margin: '0 auto',
                boxSizing: 'border-box',
                ...props.sx,
            }}
        />
    )
}

export default Content
