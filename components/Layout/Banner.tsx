import React, {FC} from "react";
import {Box, Divider, Grid, IconButton, Popover, Typography, useMediaQuery, useTheme} from "@mui/material";
import Link from "../9space-ui/Link";
import {Inter} from "next/font/google";
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';
import MenuIcon from '@mui/icons-material/Menu';
import MenuList from "@mui/material/MenuList";
import MenuItem from "@mui/material/MenuItem";

const inter = Inter({subsets: ['vietnamese']})

const Banner: FC = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);
    const handleOpenMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const open = Boolean(anchorEl);
    const id = open ? 'simple-popover' : undefined;
    const listMenu = ["Dịch vụ", "Sản phẩm/Dự án", "Quy trình", "Liên hệ"]
    return (
        <>
            <Box

                py={{xs: "8px", md: "8px", lg: "20px", xl: "20px"}} sx={{
                display: "flex",
                alignItems: "center",
                marginLeft: "auto",
                marginRight: "auto",

            }} maxWidth={{lg:"1440px", xl:"1440px"}}>
                <Box sx={{
                    display: "flex",
                    justifyContent: "flex-start"
                }}>
                    <img src={'/logo.svg'} width={"120px"} height={"40px"}/>
                </Box>
                <Box sx={{display: "flex", width: "100%", justifyContent: "flex-end", gap: "60px"}}>
                    {isMobile ? (
                        <>
                            <IconButton
                                aria-label="submit"
                                size="medium"
                                onClick={handleOpenMenu}
                            >
                                <MenuIcon sx={{color:"#FFFFFF"}}/>
                            </IconButton>
                            <Popover
                                className="popover_class"
                                id={id}
                                open={open}
                                anchorEl={anchorEl}
                                onClose={handleClose}
                                anchorPosition={{ top: 100, left: 0 }}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'center',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'center',
                                }}
                            >
                                <Box   sx={{
                                    borderRadius: "12px",
                                    background: "var(--bg-2, #181C1F)",
                                    width:"343px"
                                }}>
                                    <Box>
                                        <MenuList sx={{paddingTop:"0px"}}>
                                            <MenuItem className={inter.className}><Link href={"#"}
                                                                                      >  <Typography
                                                className={inter.className}
                                                sx={{
                                                    fontSize: "16px",
                                                    fontWeight: "600",
                                                    fontStyle: "normal",
                                                    background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 52.05%, #A2FF00 99.3%)",
                                                    backgroundClip: "text",
                                                    color: "transparent",
                                                }}>Trang chủ</Typography></Link></MenuItem>
                                            <MenuItem ><Link href={"#"} sx={{  color: "#FFFFFF",
                                                fontSize: "14px"}}>Dịch
                                                vụ</Link></MenuItem>
                                            <MenuItem><Link href={"#"}  className={inter.className} sx={{  color: "#FFFFFF",
                                                fontSize: "14px"}}>Dự
                                                án</Link></MenuItem>
                                            <MenuItem><Link href={"#"}  className={inter.className} sx={{  color: "#FFFFFF",
                                                fontSize: "14px"}}>Quy
                                                trình</Link></MenuItem>
                                            <MenuItem><Link href={"#"}  className={inter.className}  sx={{  color: "#FFFFFF",
                                                fontSize: "14px"}}>Liên
                                                hệ</Link></MenuItem>
                                        </MenuList>
                                    </Box>
                                     <Divider variant="middle" sx={{backgroundColor:"#131517"}}/>
                                    <Box sx={{padding:"22px"}}>
                                        <Box sx={{display: "flex", justifyContent: "flex-start", alignItems: "center"}}>
                                            <Box>
                                                <img src={'/icons/icon-phone.svg'}/>
                                            </Box>
                                            <Box sx={{display: "flex"}}><Typography sx={{
                                                fontSize: "14px",
                                                color: "#FFFFFF",
                                                paddingLeft: "8px"
                                            }}>Số điện thoại: </Typography>
                                                <Typography
                                                    sx={{
                                                        fontSize: "14px",
                                                        paddingLeft:"8px",
                                                        fontWeight:700,
                                                        background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 75.05%, #A2FF00 33.3%)",
                                                        backgroundClip: "text",
                                                        color: "transparent",
                                                    }}>0346 032 032</Typography></Box>

                                        </Box>

                                        <Box sx={{display: "flex", justifyContent: "flex-start", alignItems: "center",paddingTop:"20px"}}>
                                            <Box>
                                                <img src={'/icons/icon-email.svg'}/>
                                            </Box>
                                            <Box sx={{display: "flex"}}><Typography sx={{
                                                fontSize: "14px",
                                                color: "#FFFFFF",
                                                paddingLeft: "8px"
                                            }}>Email: </Typography>
                                                <Typography
                                                    sx={{
                                                        fontSize: "14px",
                                                        paddingLeft:"8px",
                                                        fontWeight:700,
                                                        background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 75.05%, #A2FF00 33.3%)",
                                                        backgroundClip: "text",
                                                        color: "transparent",
                                                    }}>sale@9space.one</Typography></Box>

                                        </Box>
                                        <Box>
                                            <Typography  sx={{color:"#D0D0D1",paddingTop:"20px"}}>Thời gian
                                                làm việc: 9h00 - 22h00 các ngày từ Thứ 2 đến Chủ nhật</Typography>
                                        </Box>
                                    </Box>


                                </Box>

                            </Popover>
                        </>
                    ) : (
                        <>
                            <Link key={"home"}
                                  href={"#"}>
                                <Typography
                                    className={inter.className}
                                    sx={{
                                        fontSize: "16px",
                                        fontWeight: "600",
                                        fontStyle: "normal",
                                        background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 52.05%, #A2FF00 99.3%)",
                                        backgroundClip: "text",
                                        color: "transparent",
                                    }}>Trang chủ</Typography>
                            </Link>
                            {
                                listMenu.map((item: any) => {
                                    return (<Link key={item}
                                                  href={"#"}>
                                        <Typography
                                            className={inter.className}
                                            sx={{
                                                fontSize: "16px",
                                                fontWeight: "600",
                                                fontStyle: "normal",
                                                color: "#FFFFFF"
                                            }}>{item}</Typography>
                                    </Link>)
                                })
                            }
                        </>
                    )}

                </Box>
            </Box>

            <Box

                maxWidth={{lg:"1660px", xl:"1660px"}}
                sx={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                marginLeft: "auto",
                marginRight: "0",

            }}
                flexDirection={{xs:"column",sm:"column", md:"column",lg:"row"}}
            >
                <Box>
                    <Box sx={{ textAlign:isMobile?"center":"left",}}>

                        <Typography
                            className={inter.className}
                            sx={{
                                fontStyle: "normal",
                                fontWeight: 700,
                                fontSize: isMobile?"28px":"56px",
                                color: "#FFFFFF",
                                display:isMobile? "inline":"block"
                            }}>Thiết kế giải pháp và phát triển</Typography>

                        <Typography className={inter.className}
                                    sx={{
                                        fontStyle: "normal",
                                        fontWeight: 700,
                                        fontSize: isMobile?"28px":"56px",
                                        background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 46.05%, #A2FF00 33.3%);",
                                        backgroundClip: "text",
                                        color: "transparent",
                                        marginTop:"4px",
                                        display:isMobile? "inline":"block",
                                        paddingLeft:isMobile?"8px":"0px"
                                    }}>Website/App</Typography>
                        <br/>
                        <Typography sx={{
                            fontStyle: "normal",
                            fontWeight: 400,
                            fontSize: "16px",
                            color: "#D0D0D1",
                            display:isMobile? "inline":"block",
                            marginTop:"16px"
                        }}>9Space mang đến thiết kế độc đáo, giải pháp phù hợp
                          </Typography>
                        <Typography sx={{
                            fontStyle: "normal",
                            fontWeight: 400,
                            fontSize: "16px",
                            color: "#D0D0D1",
                            marginTop:"12px",
                            display:isMobile? "inline":"block",
                            paddingLeft:isMobile?"8px":"0px"
                        }}>
                            với yêu cầu và trải nghiệm của từng đối tượng khách hàng, doanh nghiệp...</Typography>
                        <Box sx={{display:"flex",justifyContent:isMobile?"center":"flex-start"}}>
                            <button
                                className={inter.className}
                                style={{background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 52.05%, #A2FF00 99.3%)",
                                    width: "174px",
                                    height: "48px",
                                    borderRadius:"93px",
                                    color:"#232A2F",
                                    fontSize:"16px",
                                    fontStyle:"normal",
                                    fontWeight:"600",
                                    border:"none",
                                    marginTop:"40px",
                                    display:"flex",
                                    alignItems:"center",
                                    justifyContent:"center",
                                    cursor:"pointer",

                                }}
                                onClick={()=>alert(12312)}>
                                Liên hệ ngay <ArrowRightAltIcon sx={{marginLeft:"14px"}}/> </button>
                        </Box>

                    </Box>
                </Box>
                <Box sx={{
                    marginTop:isMobile?"48px":"0px"
                }}>
                    <img   style={{
                        // width: isMobile?"100%":"1905px",
                        // height: isMobile?"auto":"595px",
                        width: "100%",
                        height: "100%",
                        objectFit:"cover"
                    }}
                         alt={"banner-1"} src={'/images/banner-1.png'} />
                </Box>
            </Box>
            <Box
                maxWidth={{lg:"1660px", xl:"1660px"}}
                sx={{
                    display: "flex", alignItems: "center", justifyContent: "center",
                    marginLeft: "auto",
                    marginRight: "0",
                    backgroundColor:"#181C1F",
                    borderRadius:isMobile?"18px":"none"

                }}>
                <Grid  container  sx={{
                    paddingX:isMobile?"16px":"40px",
                    paddingY:isMobile?"24px":"48px"}}>
                    <Grid item xs={12} sm={12} md={12} lg={4} xl={4} >
                        <Typography  className={inter.className} sx={{
                            fontStyle: "normal",
                            fontWeight: 700,
                            fontSize: "48px",
                            background: "linear-gradient(87.93deg, #DEFEFF 1.79%, #2EE4FF 14.05%, #A2FF00 1.3%)",
                            backgroundClip: "text",
                            color: "transparent",
                        }}>30 +</Typography>
                        <Typography className={inter.className} sx={{
                            fontStyle: "normal",
                            fontWeight: 700,
                            fontSize: "20px",
                            color: "#FFFFFF"}}>Dự án</Typography>
                        <Typography sx={{
                            marginTop:"8px",
                            fontStyle: "normal",
                            fontWeight: 400,
                            fontSize: "16px",
                            color: "#D0D0D1",
                        }}>Chúng tôi đã hoàn thành nhiều dự án và <br/> nhận được phản hồi tích cực từ khách hàng.</Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={4} xl={4} >
                        <Typography className={inter.className} sx={{
                            fontStyle: "normal",
                            fontWeight: 700,
                            fontSize: "48px",
                            background: "linear-gradient(87.93deg, #DEFEFF 1.79%, #2EE4FF 14.05%, #A2FF00 1.3%)",
                            backgroundClip: "text",
                            color: "transparent",
                        }}>20 +</Typography>
                        <Typography  className={inter.className} sx={{
                            fontStyle: "normal",
                            fontWeight: 700,
                            fontSize: "20px",
                            color: "#FFFFFF"}}>Nhân lực</Typography>
                        <Typography sx={{
                            marginTop:"8px",
                            fontStyle: "normal",
                            fontWeight: 400,
                            fontSize: "16px",
                            color: "#D0D0D1",
                        }}>Chúng tôi có đội ngũ gồm nhiều chuyên gia <br/> nổi tiếng trong lĩnh vực phát triển sản phẩm.</Typography>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={4} xl={4} >
                        <Typography className={inter.className} sx={{
                            fontStyle: "normal",
                            fontWeight: 700,
                            fontSize: "48px",
                            background: "linear-gradient(87.93deg, #DEFEFF 1.79%, #2EE4FF 4.05%, #A2FF00 10.3%)",
                            backgroundClip: "text",
                            color: "transparent",
                        }}>2 +</Typography>
                        <Typography className={inter.className} sx={{
                            fontStyle: "normal",
                            fontWeight: 700,
                            fontSize: "20px",
                            color: "#FFFFFF"}}>Năm kinh nghiệm</Typography>
                        <Typography sx={{
                            marginTop:"8px",
                            fontStyle: "normal",
                            fontWeight: 400,
                            fontSize: "16px",
                            color: "#D0D0D1",
                        }}>Chúng tôi đã hoạt động được 2 năm qua và <br/>  đã nhận được sự tín nhiệm của khách hàng.</Typography>
                    </Grid>
                </Grid>
            </Box>
        </>
    )
}
export default Banner