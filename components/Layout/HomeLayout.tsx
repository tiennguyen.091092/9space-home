import React from 'react'
import Header from "./Header";
import Content from "../9space-ui/Content";
// @ts-ignore
const HomeLayout = ({ children }) => {
    return (
        <>
            <Header></Header>
            <Content>
                <div className="site-layout-background">{children}</div>
            </Content>
        </>

    )
}

export default HomeLayout// export default Id
