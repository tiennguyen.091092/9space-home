import {Inter} from "next/font/google";
import React, {FC} from "react";
import {Box, Divider, Grid, IconButton, TextField, Typography, useMediaQuery, useTheme} from "@mui/material";
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';
import Link from "../9space-ui/Link";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";

const inter = Inter({subsets: ['vietnamese']})


const Footer: FC = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    const SubmitButton = () => (
        <IconButton>
            <ArrowRightAltIcon sx={{
                width:"32px",
                height:"32px",
                marginLeft: "14px",
                borderRadius: "8px",
                background: "var(--linear, linear-gradient(90deg, #DEFEFF 0%, #2EE4FF 51.04%, #A2FF00 100%))"
            }}/>
        </IconButton>
    )

    return (
        <>
            <Box maxWidth={{lg: "1440px", xl: "1440px"}} sx={{marginLeft: "auto", marginRight: "auto"}}>
                <Grid container rowSpacing={1} sx={{justifyContent:"space-evenly"}} columnSpacing={{xs: 2, sm: 2, md: 2}}>
                    <Grid item xs={12} sm={12} md={2} lg={2} xl={2}>
                        <Box>
                            <img src={'/logo.svg'} width={"120px"} height={"40px"}/>
                        </Box>
                    </Grid>
                    <Grid item xs={12} sm={12} md={2} lg={2} xl={2}>
                        <Box>
                            <MenuList sx={{paddingTop:"0px"}}>
                                <MenuItem className={inter.className}><Link href={"#"}
                                                                            sx={{
                                                                                fontSize: "16px",
                                                                                fontWeight: 700,
                                                                                color: "#FFFFFF"
                                                                            }}>Trang chủ</Link></MenuItem>
                                <MenuItem className={inter.className}><Link href={"#"} sx={{color:"#D0D0D1"}}>Dịch
                                    vụ</Link></MenuItem>
                                <MenuItem className={inter.className}><Link href={"#"} sx={{color:"#D0D0D1"}}>Dự
                                    án</Link></MenuItem>
                                <MenuItem className={inter.className}><Link href={"#"} sx={{color:"#D0D0D1"}}>Quy
                                    trình</Link></MenuItem>
                                <MenuItem className={inter.className}><Link href={"#"} sx={{color:"#D0D0D1"}}>Liên
                                    hệ</Link></MenuItem>
                            </MenuList>

                        </Box>
                    </Grid>

                    <Grid item xs={12} sm={12} md={4} lg={4} xl={4}>
                        <Divider  sx={{backgroundColor:"#181C1F", display:isMobile?"block":"none"}} variant={"middle"} />
                        <Box>
                            <MenuList sx={{paddingTop:"0px"}}>
                                <MenuItem className={inter.className}><Typography
                                    sx={{
                                        fontSize: "16px",
                                        fontWeight: 700,
                                        color: "#FFFFFF"

                                    }}>Liên hệ</Typography></MenuItem>
                                <MenuItem className={inter.className}>
                                    <Box sx={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                                        <Box>
                                            <img src={'/icons/icon-phone.svg'}/>
                                        </Box>
                                        <Box sx={{display: "flex"}}><Typography sx={{
                                            fontSize: "14px",
                                            color: "#FFFFFF",
                                            paddingLeft: "8px"
                                        }}>Số điện thoại: </Typography>
                                            <Typography
                                                sx={{
                                                    fontSize: "14px",
                                                    paddingLeft:"8px",
                                                    fontWeight:700,
                                                    background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 75.05%, #A2FF00 33.3%)",
                                                    backgroundClip: "text",
                                                    color: "transparent",
                                                }}>0346 032 032</Typography></Box>

                                    </Box>
                                </MenuItem>
                                <MenuItem className={inter.className}>
                                    <Box sx={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                                        <Box>
                                            <img src={'/icons/icon-email.svg'}/>
                                        </Box>
                                        <Box sx={{display: "flex"}}><Typography sx={{
                                            fontSize: "14px",
                                            color: "#FFFFFF",
                                            paddingLeft: "8px"
                                        }}>Email: </Typography>
                                            <Typography
                                                sx={{
                                                    fontSize: "14px",
                                                    paddingLeft:"8px",
                                                    fontWeight:700,
                                                    background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 55.05%, #A2FF00 33.3%)",
                                                    backgroundClip: "text",
                                                    color: "transparent",
                                                }}>sale@9space.one</Typography></Box>

                                    </Box>
                                </MenuItem>
                                <MenuItem  style={{whiteSpace: 'normal'}}>
                                    <Typography sx={{color:"#D0D0D1"}} className={inter.className}>Thời gian
                                        làm việc: 9h00 - 22h00 các ngày từ Thứ 2 đến Chủ nhật</Typography>
                                </MenuItem>
                            </MenuList>

                        </Box>
                    </Grid>
                    <Grid item xs={12} sm={12} md={3} lg={3} xl={3}>
                        <Divider  sx={{backgroundColor:"#181C1F", display:isMobile?"block":"none"}} variant={"middle"} />
                        <Box>
                            <MenuList sx={{paddingTop:"0px"}}>
                                <MenuItem><Typography  className={inter.className}
                                                                            sx={{
                                                                                fontSize: "16px",
                                                                                fontWeight: 700,
                                                                                color: "#FFFFFF"

                                                                            }}>9Space</Typography></MenuItem>
                                <MenuItem  style={{whiteSpace: 'normal'}}>
                                    <Typography className={inter.className} sx={{color:"#D0D0D1"}}>Đăng ký bản tin của chúng tôi để nhận thêm thông tin và ưu đãi hơn nhé!</Typography>
                                </MenuItem>
                                <MenuItem className={inter.className}>
                                       <TextField
                                           InputProps={{
                                             style:{
                                                 color: '#FFFFFF',
                                                 borderRadius:"12px",
                                                 border: "1px solid #FFFFFF",
                                                 borderColor: '#FFFFFF',
                                             },
                                               endAdornment: <SubmitButton />
                                           }}
                                           id="standard-name"
                                           placeholder={"Nhập email của bạn"}
                                       />
                                </MenuItem>
                            </MenuList>
                        </Box>
                    </Grid>
                </Grid>
            </Box>
        </>
    )
}
export default Footer