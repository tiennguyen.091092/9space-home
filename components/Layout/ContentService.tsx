import {Inter} from "next/font/google";
import React, {FC} from "react";
import {Box, Typography, useMediaQuery, useTheme} from "@mui/material";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";

const inter = Inter({subsets: ['vietnamese']})

const ContentService: FC = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    return (<>
        <Box
            minWidth={{lg: "1440px", xl: "1440px"}}
            minHeight={{xs:"242px",sm:"242px",md:"242px",lg: "374px", xl: "374px"}}
            sx={{
                borderRadius:isMobile?"24px":"0",
                backgroundImage:
                    'url("/images/bg-service.svg")',
                backgroundSize: "container",
                backgroundRepeat: "no-repeat",
                backgroundPosition: "center",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                textAlign: "center",
               marginBottom: isMobile?"61px":"0px"
            }}>
            <Box maxWidth={{lg: "736px"}}>
                <Typography
                    className={inter.className}
                    sx={{
                        fontStyle: "normal",
                        fontWeight: 700,
                        color: "#FFFFFF",
                        fontSize: isMobile?"24px":"48px",
                    }}>Hãy trải nghiệm dịch vụ
                    của chúng tôi ngay bây giờ!</Typography>
                <Box sx={{display:"flex", justifyContent:"center"}}>
                    <button
                        className={inter.className}
                        style={{
                            background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 52.05%, #A2FF00 99.3%)",
                            width: "209px",
                            height: "48px",
                            borderRadius: "93px",
                            color: "#232A2F",
                            fontSize: "16px",
                            fontStyle: "normal",
                            fontWeight: "600",
                            border: "none",
                            marginTop: "40px",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            cursor: "pointer"
                        }}
                        onClick={() => alert(12312)}>
                        Trải nghiệm ngay <ArrowRightAltIcon sx={{marginLeft: "14px"}}/></button>
                </Box>

            </Box>

        </Box>
    </>)
}
export default ContentService