import React, {FC} from "react";
import {Box, Typography, useMediaQuery, useTheme} from "@mui/material";
import {Inter} from "next/font/google";

const inter = Inter({subsets: ['vietnamese']})
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

const ContentMobileDev: FC = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    return (
        <>

            <Box sx={{  marginTop: isMobile?"40px":"0px",
                marginBottom: isMobile?"61px":"0px"}}>
                <img style={{
                    // width: isMobile?"100%":"1905px",
                    // height: isMobile?"auto":"595px",
                    width: "100%",
                    height: "100%",
                    objectFit: "cover"
                }} alt={"img-web-dev"} src={'/images/img-mobile-dev.png'}/>
            </Box>
            <Box sx={{textAlign: isMobile ? "center" : "left",}}>
                <Typography
                    className={inter.className}
                    sx={{
                        fontStyle: "normal",
                        fontWeight: 700,
                        fontSize: isMobile ? "14px" : "16px",
                        background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 24.05%, #A2FF00 33.3%)",
                        backgroundClip: "text",
                        color: "transparent",
                        letterSpacing: "0.2em",
                        marginBottom: "8px",
                        marginTop: isMobile?"56px":"0px"
                    }}>9SPACE CÓ THỂ</Typography>
                <Typography
                    className={inter.className}
                    sx={{
                        fontStyle: "normal",
                        fontWeight: 700,
                        fontSize: isMobile ? "28px" : "48px",
                        color: "#FFFFFF",
                        display: isMobile ? "inline" : "block"
                    }}>Thiết kế & Phát triển
                </Typography>
                <Typography className={inter.className}
                            sx={{
                                fontStyle: "normal",
                                fontWeight: 700,
                                color: "#FFFFFF",
                                fontSize: isMobile ? "28px" : "48px",
                                display: isMobile ? "inline" : "block",
                            }}> Ứng dụng điện thoại</Typography>
                <Typography sx={{
                    fontStyle: "normal",
                    fontWeight: 400,
                    fontSize: "16px",
                    color: "#D0D0D1",
                    marginTop: "8px",
                }}>Với kinh nghiệm xây dựng và phát triển ứng dụng, chúng tôi tự tin đem đến cho bạn 1 sản
                    phẩm chất lượng nhất.</Typography>
                <Box sx={{
                    display: isMobile ? "flex" : "block",
                    justifyContent: isMobile ? "center" : "flex-start"
                }}>
                    <button
                        className={inter.className}
                        style={{
                            background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 52.05%, #A2FF00 99.3%)",
                            width: "236px",
                            height: "48px",
                            borderRadius: "93px",
                            color: "#232A2F",
                            fontSize: "16px",
                            fontStyle: "normal",
                            fontWeight: "600",
                            border: "none",
                            marginTop: "40px",
                            display: "flex",
                            alignItems: "center",
                            justifyContent: "center",
                            cursor: "pointer"
                        }}
                        onClick={() => alert(12312)}>
                        Liên hệ với chúng tôi <ArrowRightAltIcon sx={{marginLeft: "14px"}}/></button>
                </Box>
            </Box>
        </>
    )
}
export default ContentMobileDev