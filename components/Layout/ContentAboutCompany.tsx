import React, {FC} from "react";
import {Inter} from "next/font/google";
import {Box, Typography, useMediaQuery, useTheme} from "@mui/material";
import {Swiper, SwiperSlide} from "swiper/react";
import {Autoplay, FreeMode, Navigation, Pagination} from "swiper";
const inter = Inter({subsets: ['vietnamese']})
import "swiper/swiper-bundle.css";

const ContentAboutCompany: FC = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    const listData = [
        {
            name:"DA",
            position:"UX/UI Designer",
            content:"Tôi đã rất ấn tượng với tiến độ mà chúng tôi đã đạt được cho dự án cho đến nay. " +
                "Nhóm của chúng tôi đã thể hiện sự hợp tác và cống hiến tuyệt vời, và tôi tin tưởng rằng chúng tôi sẽ có thể cung cấp một sản phẩm chất lượng cao đúng hạn."
        },
        {
            name:"TI",
            position:"Development",
            content:"Tuy nhiên, vẫn còn một số thách thức phía trước và chúng ta cần tiếp tục tập trung và cam kết đảm bảo sự thành công của dự án này. " +
                "Tôi tin rằng với chuyên môn kết hợp và sự chăm chỉ, chúng ta sẽ có thể vượt qua mọi trở ngại và đạt được mục tiêu của mình."
        },
        {
            name:"XQ",
            position:"Product Design",
            content:"Từ giai đoạn lập kế hoạch và thiết kế ban đầu đến giai đoạn triển khai và thử nghiệm hiện tại, " +
                "tôi tự hào nói rằng nhóm của chúng tôi đã làm việc không mệt mỏi để đảm bảo rằng chúng tôi đang đi đúng hướng để cung cấp một sản phẩm hàng đầu."
        },
        {
            name:"VA",
            position:"Product Executive",
            content:"Chúng ta phải tiếp tục thúc đẩy bản thân và cố gắng đạt được sự xuất sắc trong mọi việc chúng ta làm. " +
                "Với chuyên môn tập thể và cam kết thành công của chúng tôi, tôi không nghi ngờ gì về việc chúng tôi sẽ đạt được mục tiêu của mình và vượt quá mong đợi của khách hàng."
        }
    ]
    return (
        <>
            <Box maxWidth={{lg: "1440px", xl: "1440px"}} sx={{marginLeft: "auto", marginRight: "auto"}}>
                <Box sx={{width: "100%", textAlign: "center"}}>
                    <Typography
                        className={inter.className}
                        sx={{
                            fontStyle: "normal",
                            fontWeight: 700,
                            fontSize: isMobile?"28px":"48px",
                            color: "#FFFFFF",
                            marginTop: isMobile?"56px":"0px"
                        }}>Mọi người đang nói gì về chúng tôi</Typography>
                </Box>
                <Box sx={{
                    marginTop: "48px",
                    display: "flex",
                    marginBottom: isMobile?"61px":"0px"
                }}>
                    <Swiper
                        breakpoints={{
                            0: {
                                slidesPerView: 1.5,
                                spaceBetween: 6,
                            },
                            900: {
                                slidesPerView: 4,
                                spaceBetween: 2,
                            },
                        }}
                        freeMode={true}
                        loop={true}
                        autoplay={{
                            delay: 3000,
                            disableOnInteraction: false,
                        }}
                        modules={[FreeMode, Autoplay]}
                        className="mySwiper"
                    >
                        {listData?.map((item: any, index: number) => {
                            return(
                                <SwiperSlide key={`big-banner-${index}`}>
                                    <Box maxWidth={"266px"} height="288px"
                                         sx={{
                                             border: "2px solid #353945",
                                             borderRadius: "24px",
                                             padding: "20px"
                                         }}
                                    >
                                        <Box sx={{display: "flex", alignItems: "center"}}>
                                            <Box width={"44px"} height={"44px"} sx={{backgroundColor: "#FFFFFF", borderRadius: "74px"}}>

                                            </Box>
                                            <Box paddingLeft={"8px"} >
                                                <Typography
                                                    className={inter.className}
                                                    sx={{
                                                        fontStyle: "normal",
                                                        fontWeight: 700,
                                                        fontSize: "16px",
                                                        color: "#FFFFFF"
                                                    }}>{item.name}</Typography>
                                                <Typography
                                                    className={inter.className}
                                                    sx={{
                                                        fontStyle: "normal",
                                                        fontSize: "12px",
                                                        color: "#D0D0D1"
                                                    }}>{item.position}</Typography>
                                            </Box>

                                        </Box>
                                        <Box marginTop={"20px"}
                                        >
                                            <Typography
                                                className={inter.className}
                                                sx={{
                                                    fontStyle: "normal",
                                                    fontSize: "14px",
                                                    color: "#FFFFFF",
                                                    lineHeight: isMobile?"": "28px",
                                                    letterSpacing: "0.14px"
                                                }}>{item.content} </Typography>
                                        </Box>
                                    </Box>
                                </SwiperSlide>
                            )})}
                    </Swiper>

                </Box>

            </Box>
        </>
    )
}
export default ContentAboutCompany