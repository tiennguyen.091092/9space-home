import React, {FC} from "react";
import {Box, Typography, useMediaQuery, useTheme} from "@mui/material";
import {Inter} from "next/font/google";

const inter = Inter({subsets: ['vietnamese']})
import ArrowRightAltIcon from '@mui/icons-material/ArrowRightAlt';

const ContentWebsiteDev: FC = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    return (
        <>
            <Box  sx={{ textAlign:isMobile?"center":"left",}}>
                <Typography
                    className={inter.className}
                    sx={{
                        fontStyle: "normal",
                        fontWeight: 700,
                        fontSize: "16px",
                        background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 24.05%, #A2FF00 33.3%)",
                        backgroundClip: "text",
                        color: "transparent",
                        letterSpacing: "0.2em",
                        marginBottom:"8px",
                        marginTop: isMobile?"56px":"0px"
                    }}>9SPACE CÓ THỂ</Typography>
                <Typography
                    className={inter.className}
                    sx={{
                        fontStyle: "normal",
                        fontWeight: 700,
                        color: "#FFFFFF",
                        fontSize: isMobile?"28px":"48px",
                        display:isMobile? "inline":"block"
                    }}>Dịch vụ thiết kế
                </Typography>
                <Typography className={inter.className}
                            sx={{
                                fontStyle: "normal",
                                fontWeight: 700,
                                fontSize: isMobile?"28px":"48px",
                                display:isMobile? "inline":"block",
                                color: "#FFFFFF"
                            }}>    Graphic và UI/UX</Typography>
                <Typography sx={{
                    fontStyle: "normal",
                    fontWeight: 400,
                    fontSize: "16px",
                    color: "#D0D0D1",
                    marginTop:"8px",
                }}>Chúng tôi còn cung cấp các dịch vụ thiết kế về UX/UI, Graphic Design, quy trình bàn giao file cho các ấn phẩm.</Typography>
                <Box sx={{
                    display:isMobile?"flex":"block",
                    justifyContent:isMobile?"center":"flex-start"
                }}>
                <button
                    className={inter.className}
                    style={{
                        background: "linear-gradient(87.93deg, #DEFEFF 2.79%, #2EE4FF 52.05%, #A2FF00 99.3%)",
                        width: "236px",
                        height: "48px",
                        borderRadius: "93px",
                        color: "#232A2F",
                        fontSize: "16px",
                        fontStyle: "normal",
                        fontWeight: "600",
                        border: "none",
                        marginTop: "40px",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        cursor: "pointer"
                    }}
                    onClick={() => alert(12312)}>
                    Liên hệ với chúng tôi <ArrowRightAltIcon sx={{marginLeft: "14px"}}/></button>
                </Box>
            </Box>
            <Box  sx={{  marginTop: isMobile?"40px":"0px",
                marginBottom: isMobile?"61px":"0px"}}>
                <img
                    style={{
                    width: "100%",
                    height: "100%",
                    objectFit:"cover"
                }} alt={"img-ui-ux"} src={'/images/img-ui-ux.png'}/>
            </Box>
        </>
    )
}
export default ContentWebsiteDev