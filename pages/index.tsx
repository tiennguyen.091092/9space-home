import Head from "next/head";
import {Box, useMediaQuery, useTheme} from "@mui/material";
import Banner from "../components/Layout/Banner";
import ContentWebsiteDev from "../components/Layout/ContentWebsiteDev";
import React, {FC} from "react";
// import BackgroundTopUrl from '/images/background-top.svg'
import {Inter} from "next/font/google";
// import '../styles/styles.css'
import ContentMobileDev from "../components/Layout/ContentMobileDev";
import ContentUiUx from "../components/Layout/ContentUiUx";
import ContentAboutCompany from "../components/Layout/ContentAboutCompany";
import ContentService from "../components/Layout/ContentService";
import Footer from "../components/Layout/Footer";

const inter = Inter({subsets: ['vietnamese']})

const Index: FC = () => {
    const theme = useTheme();
    const isMobile = useMediaQuery(theme.breakpoints.down("md"));
    return (
        <>
            <Head>
                <title>9Space - Out Source Company</title>
            </Head>
            <Box style={{margin: 0}}

                 className={inter.className}>
                <Box
                    paddingX={{xs: "16px", sm: "16px", md: "16px"}}
                    sx={{
                        backgroundImage:
                            'url("/images/background-top.svg")',
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "center"
                    }}>
                    <Banner></Banner>
                </Box>
                <Box height={{xs: "100%", lg: "724px"}}
                     paddingX={{xs: "16px", sm: "16px", md: "16px"}}
                     paddingY={{xs: "16px", sm: "16px", md: "16px"}}
                     flexDirection={{xs: "column", sm: "column", md: "column", lg: "row"}}
                     sx={{
                         display: "flex",
                         alignItems: "center",
                         justifyContent: "space-evenly",
                         marginLeft: "auto",
                         backgroundImage:
                             isMobile ? 'url("/images/mobile-bg-content-web-dev.svg")' : 'url("/images/bg-content-web-dev.svg")',
                         backgroundSize: "cover",
                         backgroundRepeat: "no-repeat",
                         backgroundPosition: "center",

                     }}>
                    <ContentWebsiteDev></ContentWebsiteDev>
                </Box>
                <Box
                    paddingX={{xs: "16px", sm: "16px", md: "16px"}}
                    paddingY={{xs: "16px", sm: "16px", md: "16px"}}
                    flexDirection={{xs: "column-reverse", sm: "column-reverse", md: "column-reverse", lg: "row"}}
                    height={{xs: "100%", lg: "756px"}}
                    sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-evenly",
                        marginLeft: "auto",
                        backgroundColor: "#181C1F"
                    }}>
                    <ContentMobileDev></ContentMobileDev>
                </Box>
                <Box
                    paddingX={{xs: "16px", sm: "16px", md: "16px"}}
                    paddingY={{xs: "16px", sm: "16px", md: "16px"}}
                    flexDirection={{xs: "column-reverse", sm: "column-reverse", md: "column-reverse", lg: "row"}}
                    height={{xs: "100%", lg: "752px"}}
                    sx={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "space-evenly",
                        marginLeft: "auto",
                        backgroundColor: "#131517"
                    }}>
                    <ContentUiUx></ContentUiUx>
                </Box>
                <Box
                    paddingX={{xs: "16px", sm: "16px", md: "16px"}}
                    paddingY={{xs: "16px", sm: "16px", md: "16px"}}
                    flexDirection={{xs: "column-reverse", sm: "column-reverse", md: "column-reverse", lg: "row"}}
                    height={{xs: "100%", lg: "500px"}}
                    sx={{
                        backgroundImage:
                            'url("/images/bg-about-company.svg")',
                        backgroundSize: "cover",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "center",

                    }}>
                    <ContentAboutCompany></ContentAboutCompany>
                </Box>
                <Box
                    paddingX={{xs: "16px", sm: "16px", md: "16px"}}
                    paddingY={{xs: "16px", sm: "16px", md: "16px"}}
                    flexDirection={{xs: "column-reverse", sm: "column-reverse", md: "column-reverse", lg: "row"}}
                    height={{xs: "100%", lg: "514px"}}
                     sx={{
                         backgroundColor: "#131517",
                         display: "flex",
                         justifyContent: "center",
                     }}>
                    <ContentService></ContentService>
                </Box>
                <Box
                    paddingX={{xs: "16px", sm: "16px", md: "16px"}}
                    paddingY={{xs: "16px", sm: "16px", md: "16px"}}
                    height={{xs: "100%", lg: "248px"}}
                     sx={{
                         backgroundColor: "#131517"
                     }}>
                    <Footer></Footer>
                </Box>
            </Box>
        </>

    )
}
export default Index
