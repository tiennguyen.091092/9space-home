import {NextPage} from 'next'
import {AppProps} from 'next/app'
import Head from 'next/head'
import {useRouter} from 'next/router'
import React, {useEffect} from 'react'
import {Inter} from 'next/font/google'
import {SITE_NAME, SITE_SLOGAN} from "@/constants"
import '../styles/styles.css'
import {createTheme, ThemeProvider} from "@mui/system";


const MyApp: NextPage<AppProps> = ({Component, pageProps}) => {
    const router = useRouter()
    return (
        <>
            <Head>
                <title>
                    {SITE_NAME} - {SITE_SLOGAN}
                </title>
                <meta
                    name="viewport"
                    content="minimum-scale=1, initial-scale=1, width=device-width"
                />
                <link rel="preconnect" href="https://fonts.googleapis.com"/>
                <link rel="preconnect" href="https://fonts.gstatic.com"/>
                <link
                    href="https://fonts.googleapis.com/css2?family=Fira+Sans:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap"
                    rel="stylesheet"/>
            </Head>
            <Component  {...pageProps} />
        </>
    )
}

export default MyApp
